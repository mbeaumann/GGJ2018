﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spriteAnim : MonoBehaviour {

	public SpriteRenderer render;
	public Sprite[] sprite;
	// Use this for initialization
	void Start () {
		render = GetComponent<SpriteRenderer> ();

		StartCoroutine ("anim");
	}

	IEnumerator anim(){
		int nbSprite = sprite.GetLength (0);
		int curSprite = 0;
		while (true) {
			if (curSprite >= nbSprite)
				curSprite = 0;// : curSprite = curSprite;
			render.sprite = sprite [curSprite];
			curSprite++;

			yield return new WaitForSeconds (0.1f);
		}
	}

}
