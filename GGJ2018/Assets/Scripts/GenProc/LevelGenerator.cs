﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;


public class LevelGenerator : MonoBehaviour {
	
	Grille grille;

	public int sizeX = 10;
	public int sizeY = 10;

	int indiceEntree;
	int indiceSortie;

	public GameObject[] Entrees;
	public GameObject[] Sorties;
	public GameObject[] QuatreConn;
	public GameObject[] HautGauche;
	public GameObject[] HautDroite;
	public GameObject[] BasDroite;
	public GameObject[] BasGauche;
	public GameObject[] BasHaut;
	public GameObject[] GaucheDroite;
	public GameObject[] Haut;
	public GameObject[] Bas;
	public GameObject[] Gauche;
	public GameObject[] Droite;
	public GameObject[] NULLS;


	// Use this for initialization
	void Start () {
		grille = new Grille (sizeX, sizeY);

		initTerrain (grille.grille);
		//computeOkChemin();

	}

	void initTerrain(TYPE[] tab){
		
		for (int i = 0; i < sizeY; ++i) {
			for (int j = 0; j < sizeX; ++j) {
				GameObject tmp = NULLS [0];
				if (tab[grille.index(j, i)] == TYPE.ENTREE)
					tmp = Entrees [0];
				if (tab[grille.index(j, i)] == TYPE.SORTIE)
					tmp = Sorties [0];
				if (tab[grille.index(j, i)] == TYPE.QUATRECONN)
					tmp = QuatreConn [0];
				if (tab[grille.index(j, i)] == TYPE.HAUTGAUCHE)
					tmp = HautGauche [0];
				if (tab[grille.index(j, i)] == TYPE.HAUTDROITE)
					tmp = HautDroite [0];
				if (tab[grille.index(j, i)] == TYPE.BASDROITE)
					tmp = BasDroite [0];
				if (tab[grille.index(j, i)] == TYPE.BASGAUCHE)
					tmp = BasGauche [0];
				if (tab[grille.index(j, i)] == TYPE.BASHAUT)
					tmp = BasHaut [0];
				if (tab[grille.index(j, i)] == TYPE.BAS)
					tmp = Bas [0];
				if (tab[grille.index(j, i)] == TYPE.HAUT)
					tmp = Haut [0];
				if (tab[grille.index(j, i)] == TYPE.GAUCHE)
					tmp = Gauche [0];
				if (tab[grille.index(j, i)] == TYPE.DROITE)
					tmp = Droite [0];
				if (tab[grille.index(j, i)] == TYPE.GAUCHEDROITE)
					tmp = GaucheDroite [0];

				GameObject clone = Instantiate (tmp);
				clone.transform.position = new Vector3 (j * 9, i * 9, 0);

			}
		}
	}

	void computeOkChemin(){
		bool fin = false;

		int curX = indiceEntree;
		int curY = 0;
		int dir;
		bool dirOK = false;
		while (true) {

			while (!dirOK) {
				dir = UnityEngine.Random.Range (0, 3);
				//dirOK = checkCoord (curX, curY, dir);
			}

			if (curY == sizeY - 1 && curX == indiceSortie) 
				break;
		}
	}



/*	GameObject getRandomGO(int curX, int curY, int dir){

		bool canGauche = true;
		bool canLeft = true;

	}
*/
}
