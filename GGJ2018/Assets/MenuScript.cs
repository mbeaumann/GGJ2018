﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	public void changeScene(int val){
		SceneManager.LoadScene (val);
	}

	public void quit(){
		Application.Quit ();
	}
}
