﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class porteMonte : MonoBehaviour {

	public bool up = true;
	bool dispo = true;
	public void Move(){
		if (!up)
			monte ();
		else
			descend ();
	}

	IEnumerator monteRoutine(){
		for (int i = 0; i < 200; ++i) {
			transform.Translate(Vector3.up * 0.01f);
			yield return new WaitForSeconds (0.01f);
		}
		dispo = true;
	}

	IEnumerator descendRoutine(){
		for (int i = 0; i < 200; ++i) {
			transform.Translate(-Vector3.up * 0.01f);
			yield return new WaitForSeconds (0.01f);
		}
		dispo = true;
	}

	void monte(){
		if (dispo) {
			dispo = false;
			StartCoroutine ("monteRoutine");
			up = true;
		}
	}

	void descend(){
		if (dispo) {
			dispo = false;
			StartCoroutine ("descendRoutine");
			up = false;
		}
	}
}
