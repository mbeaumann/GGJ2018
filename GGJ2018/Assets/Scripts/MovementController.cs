﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

	public float SPITcd;
	public int vie;
	public float speed;
	public float gravity;
	public float soulSpeed;
	public bool isPlayerControlled;
	float x, y;
	CharacterController	cController;
	bool grounded;
	bool jumpButtonPressed;
	bool looksToRight;
	public bool can_jump;
	public int jumpCount;
	public E_movSkill movSkill;
	public E_offSkill offSkill;
	public float turnSpeed;
	public int maxNumMinion = 5;
	public int numMinion = 0;

	public bool playerIsAgroed = false;
	public float distanceToPlayer;
	public float distanceToAggro = 25.0f;
	public float distantToPlayerMin = 1.3f;
	int hDelta;
	public float AISpeed = 5.0f;
	public bool shootAvailable;

	public GameObject projectile;
	public GameObject bulletSpawner;
	public GameObject attackHitBox;
	public GameObject possessHitBox;
	public GameObject soulPrefab;
	public GameObject minionPrefab;

	public enum E_movSkill {None, DoubleJump, Immaterial, Invisibility};
	public enum E_offSkill {None, Hit, Projectile, Summon, Possess};

	Animator anim;

	// Use this for initialization
	void Start () {
		can_jump=true;
		speed = 15.0f;
		gravity = 0.10f;
		cController = GetComponent<CharacterController>();
		turnSpeed = 360.0f;
		soulSpeed = 1500.0f;
		shootAvailable = true;
		if (gameObject.tag == "Character")
			anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		if(vie <= 0) {
			kill();
		}

		transform.position = new Vector3(transform.position.x, transform.position.y, 0);

		grounded = cController.isGrounded;
		if(movSkill == E_movSkill.Immaterial) {
			gameObject.layer = 8;
		} else {
			gameObject.layer = 0;
		}
		if(grounded) can_jump = true;
		jumpButtonPressed = Input.GetButtonDown("Jump");
		if(isPlayerControlled) {
			PlayerController();
		} else {
			AIController();
		}
	}

	void PlayerController() {
		//Horizontal movement	
		x = Input.GetAxis("Horizontal");
		//Jump 
		if(jumpButtonPressed && can_jump) {
			y = 1.0f;
			jumpCount++;
			if(jumpCount >= 2 && movSkill == E_movSkill.DoubleJump) {
				can_jump = false;
				jumpCount = 0;
			} else if (jumpCount >= 1 && movSkill != E_movSkill.DoubleJump) {
				can_jump = false;
				jumpCount = 0;
			}
		} else if (grounded) {
			y = 0.0f;
		} else {
			y -= gravity;
		}

		if (Input.GetButtonDown ("Fire1") && anim && shootAvailable) {
			anim.SetBool("attack", true);
			Invoke("animAttack", 0.1f);
		}

		//Shoot 
		if(Input.GetButtonDown("Fire1") && offSkill == E_offSkill.Projectile && shootAvailable) {
			shootAvailable = false;
			Invoke ("SPITONHIMBROTHER", 0.1f);
		}

		//Hit
		if(Input.GetButtonDown("Fire1") && offSkill == E_offSkill.Hit && shootAvailable) {
			shootAvailable = false;
			GameObject hit = Instantiate(attackHitBox, bulletSpawner.transform.position, Quaternion.identity) as GameObject;
			Destroy(hit, 0.1f);
			hit.transform.SetParent(this.transform);
			Invoke ("SPITONHIMBRUDDA", 0.5f);
		}

		//Possess
		if(Input.GetButtonDown("Fire1") && offSkill == E_offSkill.Possess) {
			GameObject pos = Instantiate(possessHitBox, bulletSpawner.transform.position, Quaternion.identity) as GameObject;
			pos.transform.SetParent(this.transform);
			Destroy(pos, 0.1f);
		}

		//DePossess
		if(Input.GetButtonDown("Fire2") && gameObject.tag == "Player") {
			GameObject soul = Instantiate(soulPrefab, transform.position, Quaternion.identity) as GameObject;
			isPlayerControlled = false;
			gameObject.tag = "Character";
		}

		//Summon minion 
		if(Input.GetButtonDown("Fire1") && offSkill == E_offSkill.Summon) {
			summon();
		}

		//Perform player movement
		cController.Move(new Vector3(x, y, 0) * Time.deltaTime * speed);
		if(Input.GetAxis("Horizontal") > 0) {
			looksToRight = true;
			gameObject.transform.localRotation =  Quaternion.Euler(new Vector3(0, 0, 0));
		}
		else if(Input.GetAxis("Horizontal") < 0) {
			looksToRight = false;
			gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
		}
	}

	void AIController() {

		//Check if player is in range of agro
		GameObject[] player = GameObject.FindGameObjectsWithTag("Player");
		if(player.Length > 0) {
			distanceToPlayer = Vector3.Distance(player[0].transform.position, this.transform.position);
			if(distanceToPlayer <= distanceToAggro && distanceToPlayer > distantToPlayerMin) {
				playerIsAgroed = true;	
			} else {
				playerIsAgroed = false;
			}
		} else {
			playerIsAgroed = false;
		}

		//If player is agroed, compute x to move towards player
		if(playerIsAgroed) {
			hDelta = (int)(player[0].transform.position.x - this.transform.position.x);
			if(hDelta > 0) {
				looksToRight = true;
				gameObject.transform.localRotation =  Quaternion.Euler(new Vector3(0, 0, 0));
				x = 1.0f;
			} else {
				looksToRight = false;
				gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
				x = -1.0f;
			}
		} else {
			//Idle
			x = 0.0f;
		}

		//Shoot
		if(shootAvailable && offSkill == E_offSkill.Projectile && playerIsAgroed) {
			shootAvailable = false;
			Invoke ("SPITONHIMBROTHER", 0.1f);
		}

		//Hit
		if(shootAvailable && offSkill == E_offSkill.Hit && playerIsAgroed) {
			shootAvailable = false;
			GameObject hit = Instantiate(attackHitBox, bulletSpawner.transform.position, Quaternion.identity) as GameObject;
			hit.transform.SetParent(this.transform);
			Destroy(hit, 0.1f);
			Invoke ("SPITONHIMBRUDDA", 0.5f);
		}

		//Summon 
		if(shootAvailable && offSkill == E_offSkill.Summon && playerIsAgroed) {
			summon();
		}

		//Gravity
		if(!grounded) {
			y -= gravity;
		} else {
			y = 0.0f;
		}

		//Perform npc movement
		cController.Move(new Vector3(x, y, 0) * Time.deltaTime * AISpeed);
	}

	void OnTriggerEnter(Collider col) {
		if(col.gameObject.tag == "Projectile" && gameObject.tag == "Character") {
			Debug.Log("Object " + gameObject.name + " Hit by a projectile !");
			vie -= 30;
			Destroy(col.gameObject);
		}

		if(col.gameObject.tag == "Projectile_NPC" && gameObject.tag == "Player") {
			Debug.Log("Object " + gameObject.name + " Hit by a projectile_npc !");
			vie -= 30;
			Destroy(col.gameObject);
		}
		if(col.gameObject.tag == "HitBox" && (gameObject.tag == "Character" || gameObject.tag == "Player")) {
			if(col.transform.parent.gameObject.tag != gameObject.tag) { //Prevent NPC from suiciding, the dirty way
				vie -= 40;
				Debug.Log("Object " + gameObject.name + " Hit by a close ranged attack from " + col.transform.parent.gameObject.tag + " !");
			}
		}
		if(col.gameObject.tag == "HitBox_Possess" && gameObject.tag == "Character") {
			GameObject[] player = GameObject.FindGameObjectsWithTag("Player");

			if(player.Length > 0) {
				Debug.Log("Someone is already possessed !");
				return;
			}
			Debug.Log("Object " + gameObject.name + " Should be possessed by " + col.transform.parent.gameObject.name);
			Destroy(col.transform.parent.gameObject);
			isPlayerControlled = true;
			gameObject.tag = "Player";
		}

		if(col.gameObject.tag == "Minion" && (gameObject.tag == "Character" || gameObject.tag == "Player")) {
			MinionController colC = (MinionController)col.gameObject.GetComponent("MinionController");
			if(colC.parentTag != gameObject.tag) {
				Debug.Log("Object " + gameObject.name + " Hit by a minion !");
				Destroy(col.gameObject);
				vie -= 100;
			}
		}
	}

	void decreaseMinionNumber() {
		if(numMinion > 0) {
			numMinion -= 1;
		}
	}

	void summon() {
		if(numMinion < maxNumMinion) {
			shootAvailable = false;
			GameObject minion = Instantiate(minionPrefab, bulletSpawner.transform.position, Quaternion.identity) as GameObject;
			Destroy(minion, 5);
			Invoke("decreaseMinionNumber", 5);
			Invoke("SPITONHIMBRUDDA", 2);
			numMinion++;
			
			MinionController mc = (MinionController)minion.GetComponent("MinionController");
			
			mc.parentTag = gameObject.tag;
			if(looksToRight) {
				minion.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
				mc.goRight = true;
			} else {
				minion.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
				mc.goRight = false;
			}

		}

	}
	void SPITONHIMBROTHER(){
		GameObject sbullet = Instantiate(projectile, bulletSpawner.transform.position, Quaternion.identity) as GameObject;
		if(gameObject.tag == "Character") {
			sbullet.tag = "Projectile_NPC";
		}
		Destroy(sbullet, 2);
		if(looksToRight) {
			sbullet.GetComponent<Rigidbody>().AddForce(new Vector3(1, 0, 0) * soulSpeed);
		} else {
			sbullet.GetComponent<Rigidbody>().AddForce(new Vector3(-1, 0, 0) * soulSpeed);
		}
		Invoke("SPITONHIMBRUDDA", SPITcd);
	}

	void SPITONHIMBRUDDA() {
		shootAvailable = true;
	}

	void animAttack(){
		anim.SetBool ("attack", false);
	}

	void kill() {
		if(gameObject.tag == "Player") {
			GameObject soul = Instantiate(soulPrefab, transform.position, Quaternion.identity) as GameObject;
		}
		Destroy(this.gameObject);
	}
}
