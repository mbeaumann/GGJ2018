﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionController : MonoBehaviour {

	public float speed;
	public bool goRight = true;
	float gravity;
	float x, y;
	public string parentTag;
	
	CharacterController cController;

	// Use this for initialization
	void Start () {
		speed = 7.0f;
		gravity = 0.1f;
		cController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(transform.position.x, transform.position.y, 0);
		if(cController.isGrounded) {
			y = 0.0f;
		} else {
			y -= gravity;
		}
		if(goRight) {
			x = 1.0f;
			gameObject.transform.localRotation =  Quaternion.Euler(new Vector3(0, 0, 0));
		} else {
			x = -1.0f;
			gameObject.transform.localRotation =  Quaternion.Euler(new Vector3(0, 180, 0));
		}

		cController.Move(new Vector3(x, y, 0) * Time.deltaTime * speed);
	}
}
