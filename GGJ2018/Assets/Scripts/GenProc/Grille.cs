﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework;

public static class dir{
	public const int GAUCHE = 0;
	public const int HAUT = 1;
	public const int DROITE = 2;
	public const int BAS = 3;
};



struct couple{
	public int i, j;
};



public class Grille {

	int sizeX;
	int sizeY;

	int indiceEntree;
	int indiceSortie;

	public TYPE[] grille;

	public Grille(int _x, int _y) {
		sizeX = _x;
		sizeY = _y;

		grille = new TYPE[100];

		initGrille ();
		computeGrille ();
	}

	public int index(int x, int y){
		return x + y * sizeX;
	}

	void initGrille(){

		indiceEntree = UnityEngine.Random.Range (0, sizeX - 1);
		indiceSortie = UnityEngine.Random.Range (0, sizeX - 1);

		for (int i = 0; i < sizeY; ++i) {
			for (int j = 0; j < sizeX; ++j) {
				TYPE tmp = TYPE.NULL;
				if (i == 0 && j == indiceEntree)
					tmp = TYPE.ENTREE;
				if (i == sizeY - 1 && j == indiceSortie)
					tmp = TYPE.SORTIE;
				grille [index (j, i)] = tmp;
			}
		}
	}

	bool checkCoord(int curX, int curY, int dir){
		bool ret = true;
		if (dir == 0 && curX <= 0) ret = false;
		if (dir == 1 && curY >= sizeY - 1) ret = false;
		if (dir == 2 && curX >= sizeX - 1) ret = false;
		if (dir == 3 && curY <= 0) ret = false;
		return ret;
	}

	void computeGrille(){
		couple init = new couple ();
		init.i = indiceEntree;
		init.j = 0;

		Queue<couple> pile = new Queue<couple> ();

		pile.Enqueue (init);

		while(pile.Count != 0){
			couple curPos = pile.Peek ();
			pile.Dequeue ();

			bool g = false;
			bool d = false;
			bool h = false;
			bool b = false;

			if (checkCoord (curPos.i, curPos.j, dir.GAUCHE))
				if(grille[index(curPos.i - 1, curPos.j)] == TYPE.NULL)
					g = Grammar.canGauche (grille [index (curPos.i, curPos.j)]);
			if(checkCoord(curPos.i, curPos.j, dir.DROITE))
				if(grille[index(curPos.i + 1, curPos.j)] == TYPE.NULL)
					d = Grammar.canDroite (grille[index(curPos.i, curPos.j)]);
			if(checkCoord(curPos.i, curPos.j, dir.HAUT))
				if(grille[index(curPos.i, curPos.j + 1)] == TYPE.NULL)
					h = Grammar.canHaut (grille[index(curPos.i, curPos.j)]);
			if(checkCoord(curPos.i, curPos.j, dir.BAS))
				if(grille[index(curPos.i, curPos.j - 1)] == TYPE.NULL)
					b = Grammar.canBas (grille[index(curPos.i, curPos.j)]);
		
			ajouteSymbol (curPos, g, d, h, b, ref pile);
		}
	}

	/*TYPE getRandomSymbol(bool g, bool d, bool b, bool h){

	}*/

	void ajouteSymbol(couple curPos, bool canGauche, bool canDroite, bool canHaut, bool canBas, ref Queue<couple> pile){

		int max = pile.Count <= 3 ? 9 : 13;
		if (canGauche) {
			couple newPos = new couple ();
			newPos.i = curPos.i - 1;
			newPos.j = curPos.j;

			bool fin = false;
			TYPE tmp = TYPE.NULL;
			int cpt = 0;
			while (!fin && cpt < 500) {
				++cpt;
				tmp = (TYPE) UnityEngine.Random.Range (3, max);

				bool testD = Grammar.canDroite (tmp);
				bool testB = Grammar.canBas (tmp);
				bool testH = Grammar.canHaut (tmp);
				bool testG = Grammar.canGauche (tmp);

				if (!checkCoord (newPos.i, newPos.j, dir.BAS) && testB)
					continue;
				if(checkCoord (newPos.i, newPos.j, dir.BAS) && testB)
					if (!Grammar.canHaut (grille [index (newPos.i, newPos.j - 1)]))
						continue;

				if (!checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)
					if (!Grammar.canBas (grille [index (newPos.i, newPos.j + 1)]))
						continue;

				if (!checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)
					if (!Grammar.canDroite (grille [index (newPos.i - 1, newPos.j)]))
						continue;
				fin = testD;
			}
			if (cpt == 500) {
				Debug.Log ("erreur");
				tmp = TYPE.NULL;
			}

			Debug.Log (tmp);
			grille [index (newPos.i, newPos.j)] = tmp;
			if (tmp != TYPE.NULL)
				pile.Enqueue (newPos);
		}

		if (canDroite) { //
			couple newPos = new couple ();
			newPos.i = curPos.i + 1;	//
			newPos.j = curPos.j;		//
			bool fin = false;
			TYPE tmp = TYPE.NULL;
			int cpt = 0;
			while (!fin && cpt < 500) {
				++cpt;
				tmp = (TYPE) UnityEngine.Random.Range (3, max);

				bool testD = Grammar.canDroite (tmp); //
				bool testB = Grammar.canBas (tmp);	  //
				bool testH = Grammar.canHaut (tmp);	  //
				bool testG = Grammar.canGauche (tmp); //

				if (!checkCoord (newPos.i, newPos.j, dir.BAS) && testB)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.BAS) && testB)	//
					if (!Grammar.canHaut (grille [index (newPos.i, newPos.j - 1)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					if (!Grammar.canGauche (grille [index (newPos.i + 1, newPos.j)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)	//
					if (!Grammar.canBas (grille [index (newPos.i, newPos.j + 1)]))
					continue;

				fin = testG;	//
			}
			if (cpt == 500) {
				Debug.Log ("erreur");
				tmp = TYPE.NULL;
			}

			Debug.Log (tmp);
			grille [index (newPos.i, newPos.j)] = tmp; //
			if (tmp != TYPE.NULL)
				pile.Enqueue (newPos);
		}

		if (canHaut) { //
			couple newPos = new couple ();
			newPos.i = curPos.i;	//
			newPos.j = curPos.j + 1;		//
			bool fin = false;
			TYPE tmp = TYPE.NULL;
			int cpt = 0;
			while (!fin && cpt < 500) {
				++cpt;
				tmp = (TYPE) UnityEngine.Random.Range (3, max);

				bool testD = Grammar.canDroite (tmp); //
				bool testB = Grammar.canBas (tmp);	  //
				bool testH = Grammar.canHaut (tmp);	  //
				bool testG = Grammar.canGauche (tmp); //


				if (!checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.HAUT) && testH)	//
					if (!Grammar.canBas (grille [index (newPos.i, newPos.j + 1)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					if (!Grammar.canGauche ( grille [index (newPos.i + 1, newPos.j)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)	//
					continue;	
				if (checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)	//
					if (!Grammar.canDroite (grille [index (newPos.i - 1, newPos.j)]))
						continue;
				
				fin = testB;	//
			}
			if (cpt == 500) {
				Debug.Log ("erreur");
				tmp = TYPE.NULL;
			}

			Debug.Log (tmp);
			grille [index (newPos.i, newPos.j)] = tmp; //
			if (tmp != TYPE.NULL)
				pile.Enqueue (newPos);
		}

		if (canBas) { //
			couple newPos = new couple ();
			newPos.i = curPos.i;	//
			newPos.j = curPos.j - 1;		//

			bool fin = false;
			TYPE tmp = TYPE.NULL;
			int cpt = 0;
			while (!fin && cpt < 500) {
				++cpt;
				tmp = (TYPE) UnityEngine.Random.Range (3, max);

				bool testD = Grammar.canDroite (tmp); //
				bool testB = Grammar.canBas (tmp);	  //
				bool testH = Grammar.canHaut (tmp);	  //
				bool testG = Grammar.canGauche (tmp); //

				if (!checkCoord (newPos.i, newPos.j, dir.BAS) && testB)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.BAS) && testB)	//
					if (!Grammar.canHaut (grille [index (newPos.i, newPos.j - 1)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.DROITE) && testD)	//
					if (!Grammar.canGauche (grille [index (newPos.i + 1, newPos.j)]))
						continue;


				if (!checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)	//
					continue;
				if (checkCoord (newPos.i, newPos.j, dir.GAUCHE) && testG)	//
					if (!Grammar.canDroite (grille [index (newPos.i - 1, newPos.j)]))
						continue;

				fin = testH;	//
			}
			if (cpt == 500) {
				Debug.Log ("erreur");
				tmp = TYPE.NULL;
			}
			Debug.Log (tmp);
			grille [index (newPos.i, newPos.j)] = tmp; //
			if (tmp != TYPE.NULL)
				pile.Enqueue (newPos);
		}
	}
}
