﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptCam : MonoBehaviour {

	public GameObject tmp;

	void Start(){
		tmp = GameObject.FindGameObjectWithTag ("Soul");
	}

	void Update () {
		if (tmp) {
			if (tmp.tag != "Player" && tmp.tag != "Soul") {
				Debug.Log ("switch");
				tmp = GameObject.FindGameObjectWithTag ("Player");
				if (!tmp)
					tmp = GameObject.FindGameObjectWithTag ("Soul");				
			}
		} else {
			tmp = GameObject.FindGameObjectWithTag ("Player");
			if (!tmp)
				tmp = GameObject.FindGameObjectWithTag ("Soul");			
		}
		transform.position = new Vector3 (tmp.transform.position.x, tmp.transform.position.y, -50);
	}
}
