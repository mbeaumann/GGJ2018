﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{

	Text text;

	void Start(){
		text = GetComponentInChildren<Text> ();
	}

	public void OnPointerEnter(PointerEventData eventData)	{
		text.color = Color.grey;
	}

	public void OnPointerExit(PointerEventData eventData)	{
		Debug.Log ("exit");
		text.color = Color.white;
	}
}
