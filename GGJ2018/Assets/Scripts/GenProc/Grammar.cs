﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TYPE {NULL = 0, ENTREE = 1, SORTIE = 2, QUATRECONN = 3, HAUTGAUCHE = 4, HAUTDROITE = 5, BASDROITE = 6, BASGAUCHE = 7, BASHAUT = 8, GAUCHEDROITE = 9, BAS = 10, HAUT = 11, GAUCHE = 12, DROITE = 13};

public class Grammar {

	public static bool canGauche(TYPE type){
		if (type == TYPE.NULL || type == TYPE.ENTREE || type == TYPE.QUATRECONN || type == TYPE.HAUTGAUCHE || type == TYPE.BASGAUCHE || type == TYPE.GAUCHE || type == TYPE.GAUCHEDROITE)
			return true;
		return false;

	}

	public static bool canDroite(TYPE type){
		if (type == TYPE.NULL || type == TYPE.ENTREE || type == TYPE.QUATRECONN || type == TYPE.HAUTDROITE || type == TYPE.BASDROITE || type == TYPE.DROITE || type == TYPE.GAUCHEDROITE)
			return true;
		return false;

	}

	public static bool canHaut(TYPE type){
		if (type == TYPE.NULL || type == TYPE.ENTREE || type == TYPE.QUATRECONN || type == TYPE.HAUTGAUCHE || type == TYPE.BASHAUT || type == TYPE.HAUTDROITE || type == TYPE.HAUT)
			return true;
		return false;

	}

	public static bool canBas(TYPE type){
		if (type == TYPE.NULL || type == TYPE.ENTREE || type == TYPE.QUATRECONN || type == TYPE.BASDROITE || type == TYPE.BASGAUCHE || type == TYPE.BASHAUT || type == TYPE.BAS)
			return true;
		return false;

	}

}
